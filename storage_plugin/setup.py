#!/usr/bin/env python3

import setuptools

with open("README.md", "r") as fh:
    desc = fh.read()

setuptools.setup(
        name="radicale_storage_laboite",
        version="0.1",
        description="MongoDB storage plugin for Radicale and Apps agenda",
        long_description=desc,
        author=" Pôle de compétences Logiciels Libres",
        author_email="eole@ac-dijon.fr",
        url="https://gitlab.mim-libre.fr/alphabet/radicale_laboite/radicale_storage_laboite",
        install_requires=["radicale_mongo",],
        packages=setuptools.find_packages(),
        include_package_data=True,
        license="GPL3+",
        classifiers=[
            "Development Status :: 1 - Beta",
            "Environment :: Plugins ",
            "Intended Audience :: System Administrators",
            "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
            "Operating System :: POSIX :: Linux",
            "Programming Language :: Python :: 3",
            "Topic :: Security",
            "Topic :: System :: Systems Administration :: Authentication/Directory",
            ],
)
